FROM postgres:10
ENV UPDATE=2018-05-13 \
    TZ=Europe/Moscow \
    LANG=ru_RU.utf8
RUN localedef -i ru_RU -c -f UTF-8 -A /usr/share/locale/locale.alias ru_RU.UTF-8
